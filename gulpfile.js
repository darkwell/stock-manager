var gulp = require('gulp'),
    sass = require('gulp-sass'),
    eslint = require('gulp-eslint'),
    handlebars = require('gulp-handlebars'),
    concat = require('gulp-concat'),
    declare = require('gulp-declare'),
    merge = require('merge2'),
    wrap = require('gulp-wrap'),
    path = require('path'),
    runSequence = require('run-sequence'),
    del = require('del'),
    rjs = require('gulp-requirejs-optimize'),
    sourcemaps = require('gulp-sourcemaps'),
    newer = require('gulp-newer'),
    livereload = require('gulp-livereload'),
    uglify = require('gulp-uglify'),
    uglifycss = require('gulp-uglifycss');

gulp.task('clean:dist', function () {
    return del('./src/main/resources/static/dist/');
});

gulp.task('sass', function () {
    return gulp.src('./src/main/resources/static/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('header.css'))
        .pipe(uglifycss())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./src/main/resources/static/dist/'))
        .pipe(gulp.dest('./target/classes/static/dist/'))
});

gulp.task('watch', function () {
    livereload.listen();

    gulp.watch(['./src/main/resources/static/app/**/*.scss', './src/main/resources/static/sass/**/*.scss'], function () {
        runSequence('sass', 'sync');
    });

    gulp.watch(['./src/main/resources/static/app/**/*.js'], function () {
        runSequence('lint', 'rjs', 'sync');
    });

    gulp.watch(['./src/main/resources/mocks/**/*.json'], ['sync']);

    return gulp.watch(['./src/main/resources/templates/runtime/**/*.hbs'], function () {
        runSequence('templates', 'sync');
    });
});

gulp.task('lint', function () {
    return gulp.src(['./src/main/resources/static/app/**/*.js'])
        .pipe(eslint())
        .pipe(eslint.format());
});

gulp.task('templates', function () {
    var helpers = gulp.src(['src/main/resources/static/app/utils/helpers/handlebars/**/*.js']);

    var partials = gulp.src(['src/main/resources/templates/runtime/**/part_*.hbs'])
        .pipe(handlebars({
            handlebars: require('handlebars')
        }))
        .pipe(wrap('Handlebars.registerPartial(<%= processPartialName(file.relative) %>, Handlebars.template(<%= contents %>));', {}, {
            imports: {
                processPartialName: function (fileName) {
                    return JSON.stringify(path.basename(fileName, '.js'));
                }
            }
        }));

    var templates = gulp.src(['src/main/resources/templates/runtime/**/*.hbs', '!src/main/resources/templates/runtime/partials/**/*.hbs'])
        .pipe(handlebars({
            handlebars: require('handlebars')
        }))
        .pipe(wrap('Handlebars.template(<%= contents %>)'));

    return merge(helpers, partials, templates)
        .pipe(declare({
            namespace: 'RW',
            noRedeclare: true
        }))
        .pipe(concat('template-bundle.js'))
        .pipe(wrap('define(["handlebars/handlebars.runtime"], function(Handlebars) {<%= contents %>; return this["RW"];});'))
        .pipe(uglify({
            preserveComments: 'license'
        }))
        .pipe(gulp.dest('src/main/resources/static/dist/'));
});

gulp.task('rjs', function () {
    return gulp.src('src/main/resources/static/app/main.js')
        .pipe(sourcemaps.init())
        .pipe(rjs({
            baseUrl: 'src/main/resources/static/app',
            namespace: 'rsHeader',
            context: null,
            mainConfigFile: 'src/main/resources/static/app/main.js',
            include: ['requirejs', 'main.js'],
            name: 'main',
            paths: {
                requirejs: '../../../../../node_modules/requirejs/require'
            },
            optimize: 'none'
        }))
        .pipe(concat('main-built.js'))
        .pipe(uglify({
            preserveComments: 'license'
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('src/main/resources/static/dist'));
});

gulp.task('uglifycss', function () {
    gulp.src('./src/main/resources/static/dist/**/*.css')
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('./dist/'));
});

gulp.task('sync', function () {
    return merge(
        gulp.src('./src/main/resources/mocks/**/*')
            .pipe(newer('./target/classes/mocks'))
            .pipe(gulp.dest('./target/classes/mocks')),
        gulp.src('./src/main/resources/templates/**/*')
            .pipe(newer('./target/classes/templates'))
            .pipe(gulp.dest('./target/classes/templates')),
        gulp.src('./src/main/resources/static/dist/**/*')
            .pipe(newer('./target/classes/static/dist'))
            .pipe(gulp.dest('./target/classes/static/dist')),
        gulp.src('./src/main/resources/static/app/**/*')
            .pipe(newer('./target/classes/static/app'))
            .pipe(gulp.dest('./target/classes/static/app'))
    ).pipe(livereload());
});

gulp.task('default', function (callback) {
    runSequence('clean:dist', ['lint', 'sass', 'templates'], 'rjs', 'sync', callback);
});

gulp.task('dev', function (callback) {
    runSequence('default', 'watch', callback);
});
