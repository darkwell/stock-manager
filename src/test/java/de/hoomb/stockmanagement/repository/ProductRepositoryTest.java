package de.hoomb.stockmanagement.repository;

import de.hoomb.stockmanagement.model.Product;
import io.prometheus.client.spring.boot.SpringBootMetricsCollector;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRepositoryTest.class);

    @Resource
    private TestEntityManager entityManager;

    @Resource
    private ProductRepository productRepository;

    @MockBean
    private SpringBootMetricsCollector springBootMetricsCollector;

    @Test
    public void testCreateProduct() {
        final UUID id = UUID.randomUUID();
        final Product product = new Product(id, "Test Product", 125, new HashSet<>(), 0);

        this.entityManager.persist(product);

        LOGGER.info("Saved Product: " + product);

        final Product savedProd = productRepository.getOne(id);

        assertThat(savedProd.getStock()).isEqualTo(125);
        assertThat(savedProd.getName()).isEqualTo("Test Product");
    }

    @Test
    public void testUpdateProduct() {
        final UUID id = UUID.randomUUID();
        final Product product = new Product(id, "Test Product", 125, new HashSet<>(), 0);

        this.entityManager.persist(product);

        LOGGER.info("Saved Product: " + product);

        final Product savedProd1 = productRepository.getOne(id);

        assertThat(savedProd1.getStock()).isEqualTo(125);
        assertThat(savedProd1.getName()).isEqualTo("Test Product");

        LOGGER.info("Change Product stock to 360");
        product.setStock(360);

        final Product savedProd2 = productRepository.getOne(id);

        assertThat(savedProd2.getStock()).isEqualTo(360);
    }
}
