package de.hoomb.stockmanagement.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hoomb.stockmanagement.model.Product;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ProductControllerTest {

    @Resource
    private MockMvc mockMvc;

    private static final UUID DEFAULT_PRODUCT_ID = UUID.fromString("dbc0f439-c5aa-480e-8b5d-776c3e2b9499");
    private static final String BASE_URL = "/api/product/";

    @Test
    public void getProductInitialStock() throws Exception {
        mockMvc.perform(get(BASE_URL + DEFAULT_PRODUCT_ID))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("\"stock\":100")));
    }

    @Test
    public void refillProduct() throws Exception {
        mockMvc.perform(put(BASE_URL + DEFAULT_PRODUCT_ID + "/refill?amount=5"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("\"stock\":105")));
    }

    @Test
    public void buyProduct() throws Exception {
        mockMvc.perform(put(BASE_URL + DEFAULT_PRODUCT_ID + "/buy"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("\"stock\":99")));
    }

    @Test
    public void buyFiveProducts() throws Exception {
        mockMvc.perform(put(BASE_URL + DEFAULT_PRODUCT_ID + "/buy?amount=5"))
            .andExpect(status().isOk())
            .andExpect(content().string(containsString("\"stock\":95")));
    }

    @Test
    public void buyProductsMoreThanStock() throws Exception {
        mockMvc.perform(put(BASE_URL + DEFAULT_PRODUCT_ID + "/buy?amount=200"))
            .andExpect(status().is4xxClientError())
            .andExpect(content().string(containsString("Not enough Product stock")));
    }

    @Test
    @Ignore
    public void optimisticLockingTest() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final String firstClient = mockMvc.perform(get(BASE_URL + DEFAULT_PRODUCT_ID))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        final String secondClient = mockMvc.perform(get(BASE_URL + DEFAULT_PRODUCT_ID))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString();

        final Product product1 = mapper.readValue(firstClient, Product.class);
        final Product product2 = mapper.readValue(secondClient, Product.class);

        System.out.println(product1);

        product1.setStock(120);
        product2.setStock(150);

        final CompletableFuture<Boolean> client1 = CompletableFuture.supplyAsync(() -> {
            try {
                mockMvc.perform(
                    patch(BASE_URL + "update")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(product1))
                ).andExpect(status().isOk());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        });


        final CompletableFuture<Boolean> client2 = CompletableFuture.supplyAsync(() -> {
            try {
                mockMvc.perform(
                    patch(BASE_URL + "update")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(mapper.writeValueAsString(product2))
                ).andExpect(status().isOk());
            } catch (Exception e) {
                e.printStackTrace();
            }

            return true;
        });

        final CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(client1, client2);

        combinedFuture.get();
    }


}
