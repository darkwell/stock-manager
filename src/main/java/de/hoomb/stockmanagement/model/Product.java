package de.hoomb.stockmanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * Models the Product object
 *
 * @author Hooman Behmanesh
 */
@Entity
@Table(name="product")
@Data
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Product {

    @Id
    @NotNull
    private UUID id;

    private String name;
    private long stock;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    @JsonManagedReference
    private Set<Reservation> reservations;

    @Version
    @JsonIgnore
    @NotNull
    private Integer version;

    @CreatedDate
    @JsonIgnore
    @NotNull
    private Date createdDate;

    @LastModifiedDate
    @JsonIgnore
    @NotNull
    private Date lastModified;


    public Product(final UUID id, final String name, final long stock, final Set<Reservation> reservations, final Integer version) {
        this.id = id;
        this.name = name;
        this.stock = stock;
        this.reservations = reservations;
        this.version = version;
    }
}
