package de.hoomb.stockmanagement.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name="reservation")
public class Reservation {

    @Id
    //@GeneratedValue(generator = "uuid2")
    //@GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;
    private long amount;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @JsonBackReference
    private Product product;

    private Date pointOfTime;

    private Reservation() {
    }

    public Reservation(final UUID id, final long amount, final Product product, final Date pointOfTime) {
        this.id = id;
        this.amount = amount;
        this.product = product;
        this.pointOfTime = pointOfTime;
    }

    public UUID getId() {
        return id;
    }

    public void setId(final UUID id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(final long amount) {
        this.amount = amount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public Date getPointOfTime() {
        return pointOfTime;
    }

    public void setPointOfTime(final Date pointOfTime) {
        this.pointOfTime = pointOfTime;
    }
}
