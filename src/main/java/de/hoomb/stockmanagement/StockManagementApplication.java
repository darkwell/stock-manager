package de.hoomb.stockmanagement;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hoomb.stockmanagement.model.Product;
import de.hoomb.stockmanagement.repository.ProductRepository;
import io.prometheus.client.spring.boot.EnablePrometheusEndpoint;
import io.prometheus.client.spring.boot.EnableSpringBootMetricsCollector;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

@SpringBootApplication
@EnablePrometheusEndpoint
@EnableSpringBootMetricsCollector
@EnableScheduling
@EnableTransactionManagement
@EnableJpaRepositories
@EnableJpaAuditing
public class StockManagementApplication {

    @Resource
    private ProductRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(StockManagementApplication.class, args);
    }

    @Bean
    InitializingBean initDatabase() {
        return () -> {
            final ObjectMapper mapper = new ObjectMapper();
            final TypeReference<List<Product>> typeReference = new TypeReference<List<Product>>() {};
            try (final InputStream inputStream = TypeReference.class.getResourceAsStream("/mock_data.json")) {
                final List<Product> products = mapper.readValue(inputStream, typeReference);

                repository.save(products);
            }

            repository.save(new Product(UUID.randomUUID(), "Test Product", 100, new HashSet<>(), 0));
        };
    }

    @Bean
    public ShallowEtagHeaderFilter shallowEtagHeaderFilter() {
        return new ShallowEtagHeaderFilter();
    }

    @Bean
    public FilterRegistrationBean shallowEtagHeaderFilterRegistration() {
        final FilterRegistrationBean result = new FilterRegistrationBean();

        result.setFilter(this.shallowEtagHeaderFilter());
        result.addUrlPatterns("/api/*");
        result.setName("shallowEtagHeaderFilter");
        result.setOrder(1);

        return result;
    }
}
