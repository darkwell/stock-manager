package de.hoomb.stockmanagement.repository;

import de.hoomb.stockmanagement.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

/**
 * Reservation Repository to execute CRUD operations on Reservation entity.
 *
 * @author Hooman Behmanesh
 */
@Repository
@Transactional
public interface ReservationRepository extends JpaRepository<Reservation, UUID> {

    @Query("SELECT r FROM Reservation r WHERE product.id = :productId AND DATEDIFF('SECOND', r.pointOfTime, CURRENT_TIMESTAMP) > :expiredSeconds")
    List<Reservation> findExpiredReservations(@Param("productId") final UUID productId, @Param("expiredSeconds") final int expiredSeconds);

    @Transactional
    @Modifying
    @Query("DELETE FROM Reservation r WHERE product.id = :productId AND DATEDIFF('SECOND', r.pointOfTime, CURRENT_TIMESTAMP) > :expiredSeconds")
    void removeExpiredProductReservations(@Param("productId") final UUID productId, @Param("expiredSeconds") final int expiredSeconds);

    @Query("SELECT r FROM Reservation r WHERE DATEDIFF('SECOND', r.pointOfTime, CURRENT_TIMESTAMP) > :expiredSeconds")
    List<Reservation> findExpiredReservations(@Param("expiredSeconds") final int expiredSeconds);
}
