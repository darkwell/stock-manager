package de.hoomb.stockmanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("de.hoomb.stockmanagement"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Stock Management",
                "Stock Management is a simple Rest-Microservice which maintains the current stock amount for a Product. The product stock can be increased (refill) or decreased (once a customer buys a product). It is also possible to acquire the current stock of a Product.\n" +
                        "The Application initializes the Database by adding a dummy Product with Id: 1 and an initial stock of 100 for test purposes.",
                "1.0",
                "Terms of service",
                new Contact("Hooman Behmanesh", "http://www.hoomb.de", "kontakt@hoomb.de"),
                "License of API", "API license URL", Collections.emptyList());
    }
}
