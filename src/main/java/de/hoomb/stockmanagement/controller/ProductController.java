package de.hoomb.stockmanagement.controller;

import com.sun.javafx.fxml.PropertyNotFoundException;
import de.hoomb.stockmanagement.model.Product;
import de.hoomb.stockmanagement.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

/**
 * Product Rest Controller
 *
 * @author Hooman Behmanesh
 */

@RestController
@RequestMapping(value = "/api/product")
@CrossOrigin
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Resource
    private ProductService productService;

    /**
     * returns the product for given Id.
     *
     * @param id Product Id which will be searched for.
     * @return found product
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation("returns the product for given Id.")
    public Product getProduct(@PathVariable("id") final UUID id) {
        return productService.getProduct(id)
            .orElseThrow(PropertyNotFoundException::new);
    }

    /**
     * returns the product for given Id.
     *
     * @param id Product Id which will be searched for.
     * @return found product
     */
    @RequestMapping(value = "/{id}/cached", method = RequestMethod.GET)
    @ApiOperation("returns the product for given Id.")
    public Product getProductCached(@PathVariable("id") final UUID id) {
        final long startTime = System.currentTimeMillis();

        LOGGER.info("Calling cached Product");

        for (int i = 0; i < 10; i++) {
            LOGGER.info("Getting cached Product {}", System.currentTimeMillis() - startTime);

            productService.getProductCached(id);
        }

        return productService.getProductCached(id)
            .orElseThrow(PropertyNotFoundException::new);
    }

    /**
     * returns all Products.
     *
     * @return all products
     */
    @RequestMapping(value = "/many", method = RequestMethod.GET)
    public List<Product> getProductAsync() {
        final List<CompletableFuture<Optional<Product>>> completableFutures = new ArrayList<>();

        for (int i = 1; i < 10; i++) {
            final CompletableFuture<Optional<Product>> productAsync = productService.getProductAsync(UUID.randomUUID());

            completableFutures.add(productAsync);
        }

        CompletableFuture.allOf(completableFutures.toArray(new CompletableFuture[completableFutures.size()])).join();

        return completableFutures.stream()
            .map(c -> {
                try {
                    return c.get();
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
                return Optional.<Product>empty();
            })
            .map(p -> p.orElse(null))
            .collect(Collectors.toList());
    }

    /**
     * returns the product stock for given Id.
     *
     * @param id Product Id which the stock will be searched for.
     * @return current product stock
     */
    @RequestMapping(value = "/{id}/stock", method = RequestMethod.GET)
    public long getProductStock(@PathVariable("id") final UUID id) {
        return productService.getProduct(id)
            .map(Product::getStock)
            .orElseThrow(PropertyNotFoundException::new);
    }

    /**
     * refills (increase) the product stock for given Id.
     *
     * @param id     Product Id which the stock will be increased.
     * @param amount number of products that must be added
     * @return changed product stock
     */
    @RequestMapping(value = "/{id}/refill", method = RequestMethod.PUT)
    public Product refillProductStock(@PathVariable("id") final UUID id, @RequestParam(value = "amount") final long amount) {
        return productService.refillProductStock(id, amount)
            .orElseThrow(PropertyNotFoundException::new);
    }

    /**
     * decreases the product stock for given Id.
     * If there are less products than available stocks, this method
     * returns an Error.
     *
     * @param id     Product Id which the stock will be increased. Default value is "1"
     * @param amount number of products that a customer buys
     * @return changed product stock
     */
    @RequestMapping(value = "/{id}/buy", method = RequestMethod.PUT)
    public Product buyProduct(@PathVariable("id") final UUID id, @RequestParam(value = "amount", defaultValue = "1") final long amount) {
        return productService.buyProduct(id, amount)
            .orElseThrow(PropertyNotFoundException::new);
    }

    @RequestMapping(value = "/{id}/reserve", method = RequestMethod.PUT)
    public Product reserveProduct(@PathVariable("id") final UUID id, @RequestParam(value = "amount", defaultValue = "1") final long amount) {
        return productService.reserveProduct(id, amount)
            .orElseThrow(PropertyNotFoundException::new);
    }

    @PatchMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Product updateProduct(@RequestBody final Product product) throws Exception {
        return productService.updateProduct(product);
    }

}
