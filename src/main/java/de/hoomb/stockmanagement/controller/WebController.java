package de.hoomb.stockmanagement.controller;

import com.sun.javafx.fxml.PropertyNotFoundException;
import de.hoomb.stockmanagement.model.Product;
import de.hoomb.stockmanagement.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Map;
import java.util.UUID;

@Controller
public class WebController {

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public String showProduct(final Map<String, Object> model) {
        final Product product = productService.getProduct(UUID.fromString("dbc0f439-c5aa-480e-8b5d-776c3e2b9499"))
            .orElseThrow(PropertyNotFoundException::new);

        model.put("product", product);

        return "product";
    }

    @GetMapping("/static")
    public String sendStatic(final Map<String, Object> model) {
        final Product product = productService.getProduct(UUID.fromString("dbc0f439-c5aa-480e-8b5d-776c3e2b9499"))
            .orElseThrow(PropertyNotFoundException::new);

        model.put("product", product);

        return "../static/index";
    }

}
